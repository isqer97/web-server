import RPi.GPIO as gpio
import time
import sys


def init():
    gpio.setmode(gpio.BOARD)
    gpio.setup(7, gpio.OUT)
    gpio.setup(11, gpio.OUT)
    gpio.setup(13, gpio.OUT)
    gpio.setup(15, gpio.OUT)


def collision():
    gpio.setmode(gpio.BOARD)

    TRIG = 31
    ECHO = 37

    gpio.setup(TRIG, gpio.OUT)
    gpio.setup(ECHO, gpio.IN)

    gpio.output(TRIG, True)
    time.sleep(0.0001)
    gpio.output(TRIG, False)

    while gpio.input(ECHO) == False:
        start = time.time()

    while gpio.input(ECHO) == True:
        end = time.time()

    sig_time = end - start

    distance = sig_time / 0.000058
    return distance


def vision():
    gpio.setmode(gpio.BOARD)
    gpio.setup(16, gpio.IN)
    gpio.setup(18, gpio.IN)
    gpio.setup(22, gpio.IN)
    gpio.setup(36, gpio.IN)

    left_sensor1 = gpio.input(16)  # lewy
    left_sensor2 = gpio.input(18)  # skrajnie lewy
    right_sensor1 = gpio.input(36)  # prawy
    right_sensor2 = gpio.input(22)  # skrajnie prawy
    return right_sensor2, right_sensor1, left_sensor1, left_sensor2
    gpio.cleanup()


def forward():
    init()
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13, False)
    gpio.output(15, True)


def reverse():
    init()
    gpio.output(7, True)
    gpio.output(11, False)
    gpio.output(13, True)
    gpio.output(15, False)


def turn_left():
    init()
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13, True)
    gpio.output(15, True)


def turn_right():
    init()
    gpio.output(7, True)
    gpio.output(11, True)
    gpio.output(13, False)
    gpio.output(15, True)


def pivot_left():
    init()
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13, True)
    gpio.output(15, False)


def pivot_right():
    init()
    gpio.output(7, True)
    gpio.output(11, False)
    gpio.output(13, False)
    gpio.output(15, True)


def cleanup():
    init()
    gpio.cleanup()


action = 'none'


def line_follow():
    while True:
        result = vision()
        distance = collision()
        if distance < 20:
            cleanup()
            continue
        if result == (0, 0, 0, 1) or result == (0, 0, 1, 1) or 
        result == (0, 0, 1, 0) or result == (0, 1, 1, 1):
            turn_left()
        elif result == (1, 0, 0, 0) or result == (1, 1, 0, 0) or 
        result == (0, 1, 0, 0) or result == (1, 1, 1, 0):
            turn_right()
        elif result == (0, 1, 1, 0):
            forward()
        elif result == (0, 0, 0, 0):
            reverse()
        else:
            continue

    cleanup()
