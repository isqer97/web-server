function zrobAjax(action) {
    $.ajax({
        url: '/',
        type: 'POST',
        data: {
            action: action
        }
    })
}

function klik1() {
    if ($('#customSwitch1').is(':checked') == true) {
        var action = 'line_follow'
        zrobAjax(action);
        console.log(action);
    }

    if ($('#customSwitch1').is(':checked') == false) {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    }
}


$(document).ready(function () {


// TELEFON <<<<<<<<<<<<<<<<<<

    $('#forward').on({
        'touchstart': function () {
            var action = 'forward'
            zrobAjax(action);
            console.log(action);
        }
    });
    $('#forward').on({
        'touchend': function () {
            var action = 'stop'
            zrobAjax(action);
            console.log(action);
        }
    });


    $('#reverse').on({
        'touchstart': function () {
            var action = 'reverse'
            zrobAjax(action);
            console.log(action);
        }
    });
    $('#reverse').on({
        'touchend': function () {
            var action = 'stop'
            zrobAjax(action);
            console.log(action);
        }
    });


    $('#turn_right').on({
        'touchstart': function () {
            var action = 'turn_right'
            zrobAjax(action);
            console.log(action);
        }
    });
    $('#turn_right').on({
        'touchend': function () {
            var action = 'stop'
            zrobAjax(action);
            console.log(action);
        }
    });


    $('#turn_left').on({
        'touchstart': function () {
            var action = 'turn_left'
            zrobAjax(action);
            console.log(action);
        }
    });
    $('#turn_left').on({
        'touchend': function () {
            var action = 'stop'
            zrobAjax(action);
            console.log(action);
        }
    });


    $('#pivot_right').on({
        'touchstart': function () {
            var action = 'pivot_right'
            zrobAjax(action);
            console.log(action);
        }
    });
    $('#pivot_right').on({
        'touchend': function () {
            var action = 'stop'
            zrobAjax(action);
            console.log(action);
        }
    });


    $('#pivot_left').on({
        'touchstart': function () {
            var action = 'pivot_left'
            zrobAjax(action);
            console.log(action);
        }
    });
    $('#pivot_left').on({
        'touchend': function () {
            var action = 'stop'
            zrobAjax(action);
            console.log(action);
        }
    });


// KOMPUTER <<<<<<<<<<<<<<

    $("#forward").mousedown(function () {
        var action = 'forward'
        zrobAjax(action);
        console.log(action);
    });
    $("#forward").mouseup(function () {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    });


    $("#reverse").mousedown(function () {
        var action = 'reverse'
        zrobAjax(action);
        console.log(action);
    });
    $("#reverse").mouseup(function () {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    });


    $("#turn_right").mousedown(function () {
        var action = 'turn_right'
        zrobAjax(action);
        console.log(action);
    });
    $("#turn_right").mouseup(function () {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    });


    $("#turn_left").mousedown(function () {
        var action = 'turn_left'
        zrobAjax(action);
        console.log(action);
    });
    $("#turn_left").mouseup(function () {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    });


    $("#pivot_right").mousedown(function () {
        var action = 'pivot_right'
        zrobAjax(action);
        console.log(action);
    });
    $("#pivot_right").mouseup(function () {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    });


    $("#pivot_left").mousedown(function () {
        var action = 'pivot_left'
        zrobAjax(action);
        console.log(action);
    });
    $("#pivot_left").mouseup(function () {
        var action = 'stop'
        zrobAjax(action);
        console.log(action);
    });


});

