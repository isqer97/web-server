import RPi.GPIO as gpio
from flask import Flask, render_template, request
from robot import Robot

app = Flask(__name__, static_url_path='')


@app.route("/", methods=['GET', 'POST'])
def main():
    if request.method == 'POST':
        action = request.form['action']
        Robot().riding(action)

    return render_template('main.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
