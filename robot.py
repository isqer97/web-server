from DefRobot import *


class Robot:

    def riding(self, action):
        if action == 'forward':
            forward()
        if action == 'reverse':
            reverse()
        if action == 'turn_right':
            turn_right()
        if action == 'turn_left':
            turn_left()
        if action == 'pivot_right':
            pivot_right()
        if action == 'pivot_left':
            pivot_left()
        if action == 'stop':
            cleanup()
        if action == 'line_follow':
            line_follow()
        if action == 'stop1':
            line_follow()
